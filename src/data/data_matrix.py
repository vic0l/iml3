from src.pipelines.generic_pipeline import perform_numerical_normalization, perform_one_hot, perform_ordinal_encode
from src.utils import file_utils
import pandas as pd
import numpy as np
import os
import re

from sklearn.impute  import SimpleImputer

class DataMatrix:
    
    def __init__(self, dataset_name,class_column,one_hot=[],ordinal=[],numerical=[]):
        """
        
        """
        self._one_hot = one_hot
        self._ordinal = ordinal
        self._numerical = numerical
        self._dataset_name = dataset_name
        self._class_column = class_column
        self._index_dict = {}
        
    
    def _get_folds_names(self):
        """
            this function will load the folds names from files
            (train_fold, test_fold)
        """
        coupled_datasets_names = []
        all_dataset_names = os.listdir('data/datasetsCBR/' + self._dataset_name)
        for name_train in all_dataset_names:
            for name_test in all_dataset_names:
                if ('train' in name_train  and 'test' in name_test 
                    and name_train.replace('train', '') == name_test.replace('test', '')):
                        coupled_datasets_names.append((name_train, name_test))
        return coupled_datasets_names

    
    def _load_dataset_folds(self):
        """
            should load arr files from path 
            and to create a list of touples with train and dest dataframes
        """
        root = f'data/datasetsCBR/{self._dataset_name}/'
        return  [
            ( int(re.findall(r'\d+',train)[0]), # index of k-fold
                file_utils.load_arff(f'{root}{train}'),
                file_utils.load_arff(f'{root}{test}')) 
        for train,test in self._get_folds_names()]



    def _join_folds(self):
        """
            should joind the test and train for each fold
            and save their indexes
        """
        joined_folds = []
        for fold_index, train, test in self._load_dataset_folds():
            self._index_dict[fold_index] = {}
            self._index_dict[fold_index]['train'] = {}
            self._index_dict[fold_index]['test'] = {}

            self._index_dict[fold_index]['train']['begin'] = 0
            self._index_dict[fold_index]['train']['end'] = train.shape[0]
            
            self._index_dict[fold_index]['test']['begin'] = train.shape[0] 
            self._index_dict[fold_index]['test']['end'] = ((train.shape[0]) + test.shape[0])
            aux = pd.concat([train,test],ignore_index=True)
            
            joined_folds.append((fold_index, aux ))
            # print(f'Joinning {aux.shape}')
        return joined_folds
    

    def _split_fold(self, index, fold):
        """
            based on indexes  saved  in {self.index_dict}
            we split each fold
        """
        train_begin =  self._index_dict[index]['train']['begin']
        train_end =  self._index_dict[index]['train']['end']

        test_begin =  self._index_dict[index]['test']['begin']
        test_end =  self._index_dict[index]['test']['end']
        
        return (fold.iloc[train_begin:train_end],fold.iloc[test_begin:test_end])


    def preprocess_folds(self):
        """
            will run the preprocessing for each fold 
            return a list of [((train_x,train_y),(test_x,text_y)), ... ,]
            order is same as in the folder
        """
        folds = []
        # 
        # string fields preprocessing
        # 
        for index, fold in self._join_folds():
            # apply to one_hot encoder and ordinal to each full fold
            # print(f'[preprocess_folds] Joinning {fold.shape}')
            one_hot_df = perform_one_hot(df=fold, colums_name=self._one_hot)
            ordinal_df = perform_ordinal_encode(df=fold, colums_name=self._ordinal)
            
            # print(f'[preprocess_folds] After one_hot_df {one_hot_df.shape}')
            # print(f'[preprocess_folds] After ordinal_df {ordinal_df.shape}')
            
            # here we remove some columns for avoiding conflicts between dataframes
            new_df  = fold.drop(self._one_hot,axis=1)
            new_df = new_df.drop(self._ordinal,axis=1)
            # print(f'[preprocess_folds] After new_df {ordinal_df.shape}')
            
            one_hot_df = one_hot_df.drop(new_df.columns, axis=1)
            one_hot_df = one_hot_df.drop(self._ordinal, axis=1)

            ordinal_df = ordinal_df.drop(self._one_hot,axis=1)
            ordinal_df = ordinal_df.drop(new_df.columns,axis=1)
            aux = ordinal_df.join(new_df).join(one_hot_df)
            # print(f'[preprocess_folds] AuxOnehot: {aux.shape}')
            folds.append((index, aux ))
       
        # 
        #   numerical fields preprocessing
        # 
        inputer = SimpleImputer(strategy='most_frequent')
        final_list = []
        for index, fold in folds:
            train_df, test_df = self._split_fold(index,fold)
           
            train_df  = perform_numerical_normalization(train_df,self._numerical)
            test_df = perform_numerical_normalization(test_df,self._numerical)

            # 
            #   training
            # 
            train_y = train_df[self._class_column]
            train_x = train_df.drop([self._class_column], axis=1)
            # this is for fixing a warning in sklearn
            train_x.columns = train_x.columns.map(lambda column:str(column))
            train_x = pd.DataFrame(inputer.fit_transform(train_x))
            
            # 
            #   testing
            # 
            test_y = test_df[self._class_column]
            # this is for fixing a warning in sklearn
            test_x = test_df.drop([self._class_column], axis=1)
            test_x.columns = test_x.columns.map(lambda column:str(column))
            test_x = pd.DataFrame(inputer.fit_transform(test_x))
        
            # print(f'After preprocess: [ train {test_x.shape}],[ test {train_x.shape}]')
            final_list.append(((train_x,train_y),(test_x,test_y)))
        return final_list
      