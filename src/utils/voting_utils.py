import numpy as np


# To decide the solution of the q, you may consider using three voting policies:
# Most voted solution, Modified Plurality, Borda count or another one that you decide.


# a. Most voted solution is the simplest method. Technically, a method for breaking ties should also be specified.
# You should decide the method in case of ties.
def most_voted():
	pass


# b. Modified Plurality computes the most voted solution but in case of ties, it removes the last k nearest neighbors and computes again
# the most voted solution. In case of ties, the process is repeated. The process finishes when there is a winner solution or when
# just one nearest neighbor remains.
def modified_plurality():
	pass


# c. Borda count voting rule, whose name comes from the French mathematician, physicist, political scientist and sailor
# Jean-Charles de Borda (1733-1799). Borda voting rule assigns k – 1 points to the solution of the most similar instance,
# k – 2 points to the second k nearest neighbor, or k – k to the solution that is ranked in k-th. The winner is the solution that
# amasses the highest total number of points. A method for breaking ties should also be specified. You should decide what to do in this case.
def bord():
	pass
