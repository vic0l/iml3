from sklearn.pipeline import Pipeline
from src.pipelines.transformers.byte_to_string_transformer import ByteToStringTransformer
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import pandas as pd

# from deprecated import deprecated

# @deprecated
def clean_data_and_transform(data_frame, numeric_columns, string_columns_ordinal, string_columns_one_hot):
	x_copy = data_frame.copy()

	x_numeric_copy = x_copy[numeric_columns]
	num_pipeline = Pipeline([
		('missing_values_median_imputer', SimpleImputer(strategy="median")),
		('standard_scaler', StandardScaler()),
	])

	x_numeric_copy = num_pipeline.fit_transform(x_numeric_copy)

	x_string_ordinal_copy = x_copy[string_columns_ordinal]
	x_string_ordinal_copy = ByteToStringTransformer(string_columns_ordinal).transform(x_string_ordinal_copy)
	x_string_ordinal_copy = OrdinalEncoder(categories='auto').fit_transform(x_string_ordinal_copy)

	# if string_columns_one_hot:
	x_string_one_hot_copy = x_copy[string_columns_one_hot]
	x_string_one_hot_copy = ByteToStringTransformer(string_columns_one_hot).transform(x_string_one_hot_copy)
	# output is a SciPy sparse matrix, instead of a NumPy array
	# this values is matrix and Idk how to add it into matrix
	x_string_one_hot_copy = OneHotEncoder().fit_transform(x_string_one_hot_copy)

	aux = np.c_[x_numeric_copy, x_string_ordinal_copy, x_string_one_hot_copy.toarray()]
	# print(numeric_columns)
	# print(string_columns_ordinal)
	# print(string_columns_one_hot)
	# print(np.append(np.append(numeric_columns, string_columns_ordinal), string_columns_one_hot))
	# aux = pd.DataFrame(aux, columns=np.append(np.append(numeric_columns, string_columns_ordinal), string_columns_one_hot))
	# print(aux.head())
	return aux


def clean_numerical_data(data_frame, numeric_columns):
	x_copy = data_frame.copy()

	x_numeric_copy = x_copy[numeric_columns]
	num_pipeline = Pipeline([
		('missing_values_median_imputer', SimpleImputer(strategy="median")),
		('standard_scaler', StandardScaler()),
	])

	return num_pipeline.fit_transform(x_numeric_copy)

def clean_text_data(data_frame, string_columns_ordinal, string_columns_one_hot):
	x_copy = data_frame.copy()

	x_string_ordinal_copy = x_copy[string_columns_ordinal]
	x_string_ordinal_copy = ByteToStringTransformer(string_columns_ordinal).transform(x_string_ordinal_copy)
	x_string_ordinal_copy = OrdinalEncoder(categories='auto').fit_transform(x_string_ordinal_copy)

	x_string_one_hot_copy = x_copy[string_columns_one_hot]
	x_string_one_hot_copy = ByteToStringTransformer(string_columns_one_hot).transform(x_string_one_hot_copy)
	# output is a SciPy sparse matrix, instead of a NumPy array
	# this values is matrix and Idk how to add it into matrix
	x_string_one_hot_copy = OneHotEncoder().fit_transform(x_string_one_hot_copy)

	aux = np.c_[x_string_ordinal_copy, x_string_one_hot_copy.toarray()]

	return aux


def perform_one_hot(df,colums_name):
	"""
		apply one-hot encoding to colums_name
		remove from datafream and append new ones
		at the end
		
		# merged = perform_one_hot(df,['workclass','education','marital-status','occupation','relationship','race','sex','native-country'])
	"""
	one_hot_df = df[colums_name]
	df_copy = df.drop(colums_name, axis=1)
	one_hot_df = ByteToStringTransformer(string_fields = colums_name).fit_transform(one_hot_df)
	x = OneHotEncoder().fit_transform(one_hot_df).toarray()
	return df_copy.join(pd.DataFrame(x))




def perform_ordinal_encode(df,colums_name):
	"""
		apply one-hot encoding to colums_name
		and append them at the end

		# perform_ordinal_encode(merged,['class'])
	"""
	ordinal_df = df[colums_name]
	df_copy = df.drop(colums_name, axis=1)
	ordinal_df = ByteToStringTransformer(string_fields = colums_name).fit_transform(ordinal_df)
	ordinal_arr = OrdinalEncoder().fit_transform(ordinal_df)
	ordinal_df =  pd.DataFrame(data=ordinal_arr, columns=colums_name)
	return df_copy.join(ordinal_df)


def perform_numerical_normalization(df,colums_name):
	"""
		apply normalization to colums_name
	"""
	dff = df.copy()
	numeric_copy = dff[colums_name]
	dff = dff.drop(colums_name,axis=1)
	
	numeric_copy = MinMaxScaler().fit_transform(numeric_copy)
	numeric_copy = StandardScaler().fit_transform(numeric_copy)
	# num_pipeline = Pipeline([
	# 	('missing_values_median_imputer', SimpleImputer(strategy="mean")),
	# 	('standard_scaler', StandardScaler()),
	# ])
	
	# numeric_copy = num_pipeline.fit_transform(numeric_copy)
	# numeric_df  = pd.DataFrame(data=numeric_copy,columns=colums_name)
	temp = np.c_[dff.to_numpy(),numeric_copy]
	temp2 = pd.DataFrame(temp,columns=(dff.columns.to_list() + colums_name))
	return temp2

# perform_numerical_normalization(merged,colums_name=['age','fnlwgt','education-num','capital-gain','capital-loss','hours-per-week'])
